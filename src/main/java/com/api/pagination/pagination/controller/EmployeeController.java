package com.api.pagination.pagination.controller;

import com.api.pagination.pagination.model.Employee;
import com.api.pagination.pagination.model.EmployeeEntity;
import com.api.pagination.pagination.service.EmployeeServiceImp;
import com.api.pagination.pagination.service.employeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("employee")
public class EmployeeController {

    @Autowired
    private EmployeeServiceImp employeeServiceImp;

    @GetMapping
    public ResponseEntity<List<EmployeeEntity>> getEmployee(@RequestParam(defaultValue = "0") Integer pageNo,
                                                            @RequestParam(defaultValue = "10") Integer pageSize,
                                                            @RequestParam(defaultValue = "id") String sortBy)
    {
        List<EmployeeEntity> list= employeeServiceImp.getEmployer(pageNo,pageSize,sortBy);
        return new ResponseEntity<List<EmployeeEntity>>(list, HttpStatus.OK);
    }
}
