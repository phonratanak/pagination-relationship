package com.api.pagination.pagination.controller;

import com.api.pagination.pagination.Repository.ValidationRepository;
import com.api.pagination.pagination.model.Relationship.requestModel.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class VaidationController {

    @Autowired
    private ValidationRepository validationRepository;

    @PostMapping("/users")
    public Users postUsers(@Valid @RequestBody Users users){
        return validationRepository.save(users);
    }
}
