package com.api.pagination.pagination.controller;

import com.api.pagination.pagination.model.Relationship.Post;
import com.api.pagination.pagination.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class PostController {
    @Autowired
    private PostService postService;

    @GetMapping("/posts")
    public List<Post> getAllPosts() {
        return postService.findAll();
    }

    @GetMapping("/posts/{id}")
    public Optional<Post> getPostById(@PathVariable Integer id) {
        return postService.findById(id);
    }

    @GetMapping("posts/user/{id}/posts")
    public List<Post> getPostByUser(@PathVariable Integer id){
        return postService.getPostByUserId(id);
    }

    @PostMapping("/post/addnew")
    public void addPost(@RequestBody Post post){
        postService.addPost(post);
    }

    @PutMapping("posts/{id}/update")
    public void updatePost(@RequestBody Post post){
        postService.updatePost(post);
    }

//    @DeleteMapping("/post/{id}/delete")
//    public void deletePost(@PathVariable Integer id)
//    {
//        postService.detelePost(id);
//    }
}
