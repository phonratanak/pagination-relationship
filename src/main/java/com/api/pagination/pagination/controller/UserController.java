package com.api.pagination.pagination.controller;

import com.api.pagination.pagination.model.Relationship.Post;
import com.api.pagination.pagination.model.Relationship.User;
import com.api.pagination.pagination.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/users")
    public List<User> getAllUsers(){
        return userService.getAllUsers();
    }

    @GetMapping("user/{id}")
    public Optional<User> getUserByid(@PathVariable int id){
        return userService.getUserByid(id);
    }

    @GetMapping("user/{id}/posts")
    public List<Post> getPostByUser(@PathVariable Integer id){
        Optional<User> user= userService.getUserByid(id);
        if(user.isPresent()){
            return user.get().getPosts();
        }
        return null;
    }

    @GetMapping("users/location/{id}/users")
    public List<User> getUserByLocation(@PathVariable Integer id){
        return userService.getUserByLocation(id);
    }

    @PostMapping("/user/addnew")
    public void addUser(@RequestBody User user){
        userService.insertUser(user);
    }

    @PutMapping("/users/{id}/update")
    public void updateUser(@RequestBody User user){
        userService.updateUser(user);
    }

    @DeleteMapping("/user/{id}/delete")
    public void deleteUser(@PathVariable Integer id){
        userService.deletePost(id);
    }
}
