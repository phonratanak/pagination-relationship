package com.api.pagination.pagination.controller;

import com.api.pagination.pagination.model.Relationship.Location;
import com.api.pagination.pagination.model.Relationship.User;
import com.api.pagination.pagination.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class LocationController {
    @Autowired
    private LocationService locationService;

    @GetMapping("/locations")
    public List<Location> getAllLocations() {
        return locationService.findAll();
    }

    @GetMapping("/locations/{id}")
    public Optional<Location> getLocationById(@PathVariable Integer id) {
        return locationService.findById(id);
    }

    @GetMapping("/location/{id}/users")
    public List<User> GetUsersByLocation(@PathVariable Integer id) {
        Optional<Location> location = locationService.findById(id);
        if(location.isPresent()) {
            return location.get().getUsers();
        }
        return null;
    }
    @PostMapping("location/addnew")
    public void postLocation(@RequestBody Location location){
        locationService.insertLocation(location);
    }

    @PutMapping("/locations/{id}/update")
    public void updateLocation(@RequestBody Location location){
        locationService.updateLocation(location);
    }

    @DeleteMapping("/post/{id}/delete")
    public void deleteLocation(@PathVariable Integer id){
        locationService.deleteLocation(id);
    }
}
