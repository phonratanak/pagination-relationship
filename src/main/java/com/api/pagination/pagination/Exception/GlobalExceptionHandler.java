package com.api.pagination.pagination.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.*;

@ControllerAdvice
public class GlobalExceptionHandler {
//    // handling specific exception
//    @ExceptionHandler(ResourceNotFoundException.class)
//    public ResponseEntity<?> resourceNotFoundHandling(ResourceNotFoundException exception, WebRequest request){
//        ErrorDetails errorDetails =
//                new ErrorDetails(new Date(), exception.getMessage(), request.getDescription(false));
//        return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
//    }

    // handling global exception

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<?> globalExceptionHandling(MethodArgumentNotValidException exception){
//        ErrorDetails errorDetails =new ErrorDetails(new Date(), "Validation Field", exception.getBindingResult().getFieldError().getDefaultMessage());
        List<Object> errors = new ArrayList<>();
        exception.getBindingResult().getAllErrors().forEach(
                (error)->{
                Map<String,String> objectError=new HashMap<>();
                String filename=((FieldError)error).getField();
                String errorMessage=error.getDefaultMessage();
                objectError.put("field",filename);
                objectError.put("message",errorMessage);
                errors.add(objectError);
        }
        );
        ErrorDetails errorDetails =new ErrorDetails(new Date(), "Validation Field", errors);
        return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
