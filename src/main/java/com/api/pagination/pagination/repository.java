package com.api.pagination.pagination;

import com.api.pagination.pagination.model.EmployeeEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface repository extends PagingAndSortingRepository<EmployeeEntity,Long>{
}
