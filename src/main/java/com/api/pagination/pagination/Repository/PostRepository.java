package com.api.pagination.pagination.Repository;

import com.api.pagination.pagination.model.Relationship.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post,Integer> {
    List<Post> findByUserId(Integer id);
}
