package com.api.pagination.pagination.Repository;

import com.api.pagination.pagination.model.Relationship.requestModel.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ValidationRepository extends JpaRepository<Users,Integer> {
}
