package com.api.pagination.pagination.Repository;

import com.api.pagination.pagination.model.Relationship.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationsRepository extends JpaRepository<Location,Integer> {
}
