package com.api.pagination.pagination.Repository;

import com.api.pagination.pagination.model.Relationship.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User,Integer> {
    List<User> findByLocationId(Integer id);
}
