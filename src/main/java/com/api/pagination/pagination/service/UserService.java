package com.api.pagination.pagination.service;

import com.api.pagination.pagination.Repository.UserRepository;
import com.api.pagination.pagination.model.Relationship.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public Optional<User> getUserByid(int id) {
        return userRepository.findById(id);
    }

    public List<User> getUserByLocation(Integer id) {
        return userRepository.findByLocationId(id);
    }

    public void insertUser(User user){
        userRepository.save(user);
    }

    public void updateUser(User user) {
        userRepository.save(user);
    }

    public void deletePost(Integer id) {
        userRepository.deleteById(id);
    }
}
