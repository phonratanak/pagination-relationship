package com.api.pagination.pagination.service;

import com.api.pagination.pagination.model.EmployeeEntity;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface employeeService {
    List<EmployeeEntity> getEmployer(Integer pageNo, Integer pageSize, String sortBy);
}
