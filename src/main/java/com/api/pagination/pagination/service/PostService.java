package com.api.pagination.pagination.service;

import com.api.pagination.pagination.Repository.PostRepository;
import com.api.pagination.pagination.model.Relationship.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PostService {

    @Autowired
    private PostRepository postRepository;

    public List<Post> findAll() {
        return (List<Post>) postRepository.findAll();
    }

    public Optional<Post> findById(Integer id) {
        return postRepository.findById(id);
    }

    public List<Post> getPostByUserId(Integer id) {
        return postRepository.findByUserId(id);
    }

    public void addPost(Post post){
        postRepository.save(post);
    }

    public void updatePost(Post post) {
        postRepository.save(post);
    }


    public void detelePost(Integer id) {
        postRepository.deleteById(id);
    }
}
